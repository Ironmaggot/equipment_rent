System Requirements:
[*]Ensure that you Visual Studio is at least Version 15.9.*
[*]Ensure that your Visual Studio has Asp.Net.Core 2.2 installed.
[*]Ensure that you have Package Manager Console 3.0 installed.

Running:
[*]Press F5 while in Visual Studio.