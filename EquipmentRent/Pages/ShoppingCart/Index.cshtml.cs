﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using EquipmentRent.Models;
using EquipmentRent.Data;

namespace EquipmentRent.Pages.ShoppingCart
{
    public class IndexModel : PageModel
    {
        private readonly EquipmentRent.Models.EquipmentRentContext _context;

        public IndexModel(EquipmentRent.Models.EquipmentRentContext context)
        {
            _context = context;
        }

        public IList<CartItem> CartItem { get;set; }

        public async Task OnGetAsync()
        {
            CartItem = await _context.CartItem
                .Include(i => i.Item)
                .ToListAsync();
        }

        public ActionResult OnPostGetInvoice()
        {
            MemoryStream memoryStream = new MemoryStream();
            TextWriter textWriter = new StreamWriter(memoryStream);
            decimal totalCost = 0;
            int totalLoyalityPoints = 0;
            string separator = " | ";

            textWriter.WriteLine("INVOICE");
            textWriter.WriteLine("");

            foreach (var item in _context.CartItem.Include(i => i.Item).ToArray())
            {
                decimal itemCost = CalculateCost(item.Item.Type, item.RentLength);
                totalCost = totalCost + itemCost;

                int loyalityPoints = LoyalityPoints(item.Item.Type);
                totalLoyalityPoints = totalLoyalityPoints + loyalityPoints;

                string name = item.Item.Name;
                string length = item.RentLength.ToString() + " days";
                string cost = itemCost.ToString("F") + " euros";
                
                string itemString = name + separator + length + separator + cost;

                textWriter.WriteLine(itemString);
            }
            textWriter.WriteLine("");
            textWriter.WriteLine("TOTAL: " + totalCost.ToString("F") + " euros");
            textWriter.WriteLine("LOYALITY POINTS GAINED: " + "+" + totalLoyalityPoints.ToString());

            textWriter.Flush();
            memoryStream.Position = 0;

            return File(memoryStream, "application/force-download", "myFile.txt");
        }

        private decimal CalculateCost(string type, int rentLength)
        {
            decimal oneTimeFee = 100;
            decimal premiumDailyFee = 60;
            decimal regularDailyFee = 40;
            decimal cost = 0;

            //One time fee
            if (type != "Specialized")
            {
                cost = cost + oneTimeFee;
            }

            if (type == "Heavy")
            {
                decimal premiumFee = premiumDailyFee * rentLength;
                cost = cost + premiumFee;
            }
            else if (type == "Regular")
            {
                if (rentLength > 2)
                {
                    decimal premiumFee = premiumDailyFee * 2;
                    int adjustedLength = rentLength - 2;
                    decimal regularFee = adjustedLength * regularDailyFee;

                    cost = cost + premiumFee + regularFee;
                }
                else
                {
                    decimal premiumFee = rentLength * premiumDailyFee;

                    cost = cost + premiumFee;
                }
            }
            else if (type == "Specialized")
            {
                if (rentLength > 3)
                {
                    decimal premiumFee = premiumDailyFee * 3;
                    int adjustedLength = rentLength - 3;
                    decimal regularFee = adjustedLength * regularDailyFee;

                    cost = cost + premiumFee + regularFee;
                }
                else
                {
                    decimal premiumFee = premiumDailyFee * rentLength;

                    cost = cost + premiumFee;
                }
            }

            return cost;
        }

        private int LoyalityPoints(string type)
        {
            if (type == "heavy")
            {
                return 2;
            }
            else
            {
                return 1;
            }
        }
    }
}
