﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using EquipmentRent.Models;

namespace EquipmentRent.Pages.Catalogue
{
    public class IndexModel : PageModel
    {
        private readonly EquipmentRent.Models.EquipmentRentContext _context;

        public IndexModel(EquipmentRent.Models.EquipmentRentContext context)
        {
            _context = context;
        }

        public IList<Item> Item { get;set; }
        public CartItem CartItem { get; set; }
        public int ItemID { get; set; }
        public int RentLength { get; set; }

        public async Task OnGetAsync()
        {
            Item = await _context.Item.ToListAsync();
        }

        public async Task<IActionResult> OnPost(int? id, int rentLength)
        {
            if (id != null)
            {
                ItemID = id.Value;
            }

            var item = _context.Item.Find(ItemID);
            var cartItem = new CartItem() { Item = item, RentLength = rentLength };
            _context.CartItem.Add(cartItem);
            _context.SaveChanges();

            return RedirectToPage("./Index");
        }
    }
}
