﻿using EquipmentRent.Models;
using System;
using System.Linq;

namespace EquipmentRent.Data
{
    public class DbInitializer
    {
        public static void Initialize(EquipmentRentContext context)
        {
            if (context.Item.Any())
            {
                return; //Database is already seeded
            }

            var items = new Item[]
            {
                new Item{Name="Caterpillar bulldozer", Type="Heavy"},
                new Item{Name="KamAZ truck", Type="Regular"},
                new Item{Name="Komatsu crane", Type="Heavy"},
                new Item{Name="Volvo steamroller", Type="Regular"},
                new Item{Name="Bosch jackhammer", Type="Specialized"}
            };

            foreach (Item i in items)
            {
                context.Item.Add(i);
            }
            context.SaveChanges();
        }
    }
}
