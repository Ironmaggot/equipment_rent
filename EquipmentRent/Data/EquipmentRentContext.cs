﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EquipmentRent.Models;

namespace EquipmentRent.Models
{
    public class EquipmentRentContext : DbContext
    {
        public EquipmentRentContext (DbContextOptions<EquipmentRentContext> options)
            : base(options)
        {
        }

        public DbSet<EquipmentRent.Models.Item> Item { get; set; }

        public DbSet<EquipmentRent.Models.CartItem> CartItem { get; set; }
    }
}
