﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EquipmentRent.Migrations
{
    public partial class RentLength : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CartItem_Item_ItemID",
                table: "CartItem");

            migrationBuilder.AlterColumn<int>(
                name: "ItemID",
                table: "CartItem",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RentLength",
                table: "CartItem",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_CartItem_Item_ItemID",
                table: "CartItem",
                column: "ItemID",
                principalTable: "Item",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CartItem_Item_ItemID",
                table: "CartItem");

            migrationBuilder.DropColumn(
                name: "RentLength",
                table: "CartItem");

            migrationBuilder.AlterColumn<int>(
                name: "ItemID",
                table: "CartItem",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_CartItem_Item_ItemID",
                table: "CartItem",
                column: "ItemID",
                principalTable: "Item",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
