﻿// <auto-generated />
using System;
using EquipmentRent.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EquipmentRent.Migrations
{
    [DbContext(typeof(EquipmentRentContext))]
    [Migration("20190407093941_CartItem")]
    partial class CartItem
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.3-servicing-35854")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("EquipmentRent.Models.CartItem", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("ItemID");

                    b.HasKey("ID");

                    b.HasIndex("ItemID");

                    b.ToTable("CartItem");
                });

            modelBuilder.Entity("EquipmentRent.Models.Item", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("Item");
                });

            modelBuilder.Entity("EquipmentRent.Models.CartItem", b =>
                {
                    b.HasOne("EquipmentRent.Models.Item", "Item")
                        .WithMany()
                        .HasForeignKey("ItemID");
                });
#pragma warning restore 612, 618
        }
    }
}
