﻿using System;
using System.Collections.Generic;

namespace EquipmentRent.Models
{
    public class Item
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
