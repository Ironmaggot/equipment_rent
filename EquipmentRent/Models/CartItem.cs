﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EquipmentRent.Models
{
    public class CartItem
    {
        public int ID { get; set; }
        [Required]
        public int RentLength { get; set; }
        [Required]
        public Item Item { get; set; }
    }
}
